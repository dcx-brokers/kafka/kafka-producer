#Producteur de messages Kafka
Le but du projet est de produire des messages _complexes_, et de les émettre vers un serveur ou un cluster Apache Kafka / Confluent Kafka

## Préparation
Il faut copier le fichier `config/producer.properties`, et adapter les valeurs.  
  * `topic` est le topic dans lequel le producer doit écrire.
  * `bootstrap.servers` indique le ou les serveurs qui forment le cluster Kafka.
  * `producer.client.id` indique quel client produit les messsages. Il est automatiquement suffixé par le numéro courant du thread.
  * `thread.number` indique le nombre de threads à lancer en parallèle.

## Lancement
Le fichier `doc/kafka-producer.jar` doit être copié dans le même répertoire que le fichier `producer.properties`.  
La commande à lancer est
  * `java -cp kafka-producer.jar com.cgi.brokers.kafka.producer.CustomKafkaProducer [--message-count NUMBER] [--message-size MESSAGE_SIZE]`  

### Paramétrage lors de l'exécution
  * Le paramètre `message-count` est facultatif. S'il n'est pas présent, la valeur vaut 5, sinon la velaur passée en paramètre. Ce nombre correspond au nombre de messages à générer.  
  * Le paramètre `message-size` est facultatif. S'il n'est pas présent, la valeur vaut 1000, sinon la velaur passée en paramètre. Ce nombre correspond à la taille du payload à envoyer dans le message.

## Détails
Durant l'exécution du programme, il va indiquer l'état d'avancement tous les dix pourcents, ainsi que le temps global d'exécution à la fin de la production de messages.

## Objet créé lors de la production
Afin de présenter la production avec un objet _complexe_, le format de l'objet utilisé est le suivant :
  * `Identifiant` : l'identifiant est sous la forme Timestamp(ms)/UUID
  * `Message` : le message émis sous forme de String (chaine aléatoire à chaque message)
  * `currentDate` : la date de création du message

## Fonctionnement
Le fonctionnement du projet est le suivant : on crée `N` messages que l'on envoie un par un au serveur ou au cluster Kafka.

### Convertisseur de messages
Afin que les messages soient sérialisés à la volée, avant l'envoi vers le serveur ou le cluster Kafka, on crée une classe de sérialisation.  
Cette classe doit implémenter l'interface `org.apache.kafka.common.serialization.Serializer<?>` qui doit être typée avec le type de messages que l'on veut sérialiser.  
