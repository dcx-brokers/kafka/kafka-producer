package com.cgi.brokers.kafka.producer.properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;

/**
 * Loader de fichiers properties
 *
 * @author damien.cacheux
 */
public class PropertiesLoader
{
    /**
     * Logger de la classe.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesLoader.class);

    private Properties properties;

    public PropertiesLoader withFiles(final String... theFiles)
    {
        Preconditions.checkNotNull(theFiles, "Le paramètre 'theFiles' ne doit pas être null");
        if (this.properties == null)
        {
            this.properties = new Properties();
        }

        for (final String file : theFiles)
        {
            try
            {
                this.properties.load(new FileInputStream(file));
            } catch (final IOException theE)
            {
                PropertiesLoader.LOGGER.error(String.format("Erreur lors du chargement du fichier %s : %s", file, theE.getMessage()));
            }
        }

        return this;
    }
    
    public Properties get()
    {
        return this.properties;
    }
}
