package com.cgi.brokers.kafka.producer.model;

import java.util.Date;

import com.cgi.brokers.kafka.producer.builder.ConfigurationBuilder;
import com.google.common.base.MoreObjects;

import net.karneim.pojobuilder.GeneratePojoBuilder;

/**
 * Message complexe à envoyer.
 *
 * @author damien.cacheux
 */
@GeneratePojoBuilder(withSetterNamePattern = "*", withBuilderInterface = ConfigurationBuilder.class, withBuilderProperties = true, withCopyMethod = true)
public class ComplexMessage
{
    /**
     * Identifiant du message
     */
    private String identifiant;

    /**
     * Contenu du message
     */
    private String message;

    /**
     * Date d'émission du message
     */
    private Date currentDate;

    /**
     * Accesseur de {@link #identifiant}.
     *
     * @return la valeur de identifiant
     */
    public String getIdentifiant()
    {
        return this.identifiant;
    }

    /**
     * Mutateur de {@link #identifiant}.
     *
     * @param theIdentifiant le identifiant à affecter
     */
    public void setIdentifiant(final String theIdentifiant)
    {
        this.identifiant = theIdentifiant;
    }

    /**
     * Accesseur de {@link #message}.
     *
     * @return la valeur de message
     */
    public String getMessage()
    {
        return this.message;
    }

    /**
     * Mutateur de {@link #message}.
     *
     * @param theMessage le message à affecter
     */
    public void setMessage(final String theMessage)
    {
        this.message = theMessage;
    }

    /**
     * Accesseur de {@link #currentDate}.
     *
     * @return la valeur de currentDate
     */
    public Date getCurrentDate()
    {
        return this.currentDate;
    }

    /**
     * Mutateur de {@link #currentDate}.
     *
     * @param theCurrentDate le currentDate à affecter
     */
    public void setCurrentDate(final Date theCurrentDate)
    {
        this.currentDate = theCurrentDate;
    }

    @Override
    public String toString()
    {
        return MoreObjects.toStringHelper(this)
                .add("identifiant", this.identifiant)
                .add("message", this.message)
                .add("currentDate", this.currentDate)
                .toString();
    }
}
