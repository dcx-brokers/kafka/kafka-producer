package com.cgi.brokers.kafka.producer.model.serializer;

import java.util.Map;

import org.apache.kafka.common.serialization.Serializer;

import com.cgi.brokers.kafka.producer.model.ComplexMessage;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Serializer pour un {@link ComplexMessage}.
 *
 * @author damien.cacheux
 */
public class ComplexMessageSerializer implements Serializer<ComplexMessage>
{
    @Override public void configure(final Map<String, ?> theMap, final boolean theB)
    {

    }

    @Override
    public byte[] serialize(final String theS, final ComplexMessage theComplexMessage)
    {
        byte[] retVal = null;
        final ObjectMapper objectMapper = new ObjectMapper();
        try
        {
            retVal = objectMapper.writeValueAsString(theComplexMessage).getBytes();
        } catch (final Exception e)
        {
            e.printStackTrace();
        }
        return retVal;
    }

    @Override public void close()
    {

    }
}
