package com.cgi.brokers.kafka.producer.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

public class CustomKafkaProducerOptionParser
{
    /**
     * Logger de la classe.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomKafkaProducerOptionParser.class);

    private final CustomKafkaProducerContext customKafkaProducerContext;

    /**
     * Constructeur.
     *
     * @param args Paramètres de la chaine de caractères
     */
    public CustomKafkaProducerOptionParser(final String[] args)
    {
        CustomKafkaProducerOptionParser.LOGGER.info(String.format("Gestion des paramètres %s", (Object) args));

        final OptionParser optionParser = new OptionParser();

        final OptionSpec<Integer> nbMessagesOption = optionParser
                .accepts("message-count", "Nombre de messages à émettre")
                .withOptionalArg()
                .ofType(Integer.class)
                .defaultsTo(5);

        final OptionSpec<Integer> tailleMessagesOption = optionParser
                .accepts("message-size", "Taille de la payload des messages à émettre")
                .withOptionalArg()
                .ofType(Integer.class)
                .defaultsTo(1000);

        final OptionSet options = optionParser.parse(args);

        this.customKafkaProducerContext = new CustomKafkaProducerContextBuilder()
                .nbMessages(options.valueOf(nbMessagesOption))
                .tailleMessages(options.valueOf(tailleMessagesOption))
                .build();
    }

    /**
     * Accesseur de {@link #customKafkaProducerContext}.
     *
     * @return la valeur de customKafkaProducerContext
     */
    public CustomKafkaProducerContext getCustomKafkaProducerContext()
    {
        return this.customKafkaProducerContext;
    }
}
