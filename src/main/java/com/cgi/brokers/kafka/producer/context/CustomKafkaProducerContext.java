package com.cgi.brokers.kafka.producer.context;

import java.io.Serializable;

import com.cgi.brokers.kafka.producer.builder.ConfigurationBuilder;

import net.karneim.pojobuilder.GeneratePojoBuilder;

/**
 * Contexte du CustomKafkaProducer.
 * <pre>Il permet de paramétrer le comportement du batch (nombre de messages à émettre, et taille des messages).
 * Les valeurs par défaut, si rien n'est précisé, sont 5 messages avec une taille de 1000 caractères pour chaque.</pre>
 *
 * @author damien.cacheux
 */
@GeneratePojoBuilder(withSetterNamePattern = "*", withBuilderInterface = ConfigurationBuilder.class, withBuilderProperties = true, withCopyMethod = true)
public class CustomKafkaProducerContext implements Serializable
{
    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -3960386975909468942L;

    /**
     * Nombre de messages à envoyer (5 par défaut).
     */
    private Integer nbMessages = 5;

    /**
     * Taille des messages à envoyer (1000 par défaut).
     */
    private Integer tailleMessages = 1000;

    /**
     * Accesseur de {@link #nbMessages}.
     *
     * @return la valeur de nbMessages
     */
    public Integer getNbMessages()
    {
        return this.nbMessages;
    }

    /**
     * Mutateur de {@link #nbMessages}.
     *
     * @param theNbMessages le nbMessages à affecter
     */
    public void setNbMessages(final Integer theNbMessages)
    {
        this.nbMessages = theNbMessages;
    }

    /**
     * Accesseur de {@link #tailleMessages}.
     *
     * @return la valeur de tailleMessages
     */
    public Integer getTailleMessages()
    {
        return this.tailleMessages;
    }

    /**
     * Mutateur de {@link #tailleMessages}.
     *
     * @param theTailleMessages le tailleMessages à affecter
     */
    public void setTailleMessages(final Integer theTailleMessages)
    {
        this.tailleMessages = theTailleMessages;
    }
}
